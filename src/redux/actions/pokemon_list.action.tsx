import { ActionCreator } from 'redux';
import http, { URL_BASE } from '../../services/http';
import { PokemonListActionTypes, SET_POKEMON_LIST, PokemonListResponseItem, SET_TYPE_FILTER, SET_LOADING, SET_NEXT_PREV_PATH, PokemonListResponse, PokemonInterface, CATCH_POKEMON, BREAK_FREE_POKEMON, SET_POKEMONS_AUTOCOMPLETE, PokemonItemAutocomplete } from '../types';

const setListPokemon: ActionCreator<PokemonListActionTypes> = (pokemons: PokemonListResponseItem[]) => {
    return {type: SET_POKEMON_LIST, payload: pokemons};
} 

export const updateLoadingAction : ActionCreator<PokemonListActionTypes> = ({status}:{status: boolean}) => {
    return {type:SET_LOADING, payload: status};
}

const setNextPrevPath: ActionCreator<PokemonListActionTypes> = ({next, prev}: {next: string, prev: string})=>{
    return {type: SET_NEXT_PREV_PATH, payload: {next, prev}};
}

export const fetchInitPokemonList = ({path}: {path: string})=>{
    return async (dispatch: any) => {
        const pokemonRequest : PokemonListResponse = await http.get('pokemon');
        const pokemonList: PokemonListResponseItem[] = pokemonRequest.results;
        console.log('Pokemon request', pokemonRequest);

        const next: string | null | undefined = Boolean(pokemonRequest.next) ? pokemonRequest.next?.toString().split(URL_BASE)[1] : null;
        const prev: string | null | undefined = Boolean(pokemonRequest.previous) ? pokemonRequest.previous?.toString().split(URL_BASE)[1]: null;

        dispatch(setNextPrevPath({next, prev}))
        return dispatch(setListPokemon(pokemonList))
    }
}

export const fetchTypePokemonList = ({path, typeFilter}: {path: string | null, typeFilter: string})=>{
    return async (dispatch: any) => {

        dispatch(updateLoadingAction({status: true}));
        const pathUrl = Boolean(path) ? path : typeFilter;
        console.log('pathUrl ',pathUrl) 
        const pokemonRequest: PokemonListResponse = await http.get(pathUrl!);
        console.log('Pokemon result ', typeFilter, pokemonRequest);
        const next: string | null | undefined = Boolean(pokemonRequest.next) ? pokemonRequest.next?.toString().split(URL_BASE)[1] : null;
        const prev: string | null | undefined = Boolean(pokemonRequest.previous) ? pokemonRequest.previous?.toString().split(URL_BASE)[1]: null;
        dispatch(setNextPrevPath({next, prev}))
        const pokemonList: PokemonListResponseItem[] = pokemonRequest.results;
        return dispatch(setListPokemon(pokemonList))
    }
}

export const getAllPokemonsForAutoComplete = ()=> async (dispatch : any) => {
    const pokemonRequest : PokemonListResponse = await http.get('pokemon?limit=2000');
    const pokemonList: PokemonListResponseItem[] = pokemonRequest.results;
    const pokemonsListAutocomplete : PokemonItemAutocomplete[] = await pokemonList.map((item : PokemonListResponseItem) => ({id: item.name, title: item.name}));
    dispatch({type: SET_POKEMONS_AUTOCOMPLETE, payload: pokemonsListAutocomplete})
    console.log('Data pokemon',pokemonRequest);
}

export const loadCategoryPokemons = ({url} : {url : string}) => async (dispatch: any) => {
    const pokemonRequest : any = await http.get(url);
    const pokemonList: PokemonListResponseItem[] = pokemonRequest.pokemon.map((item: any)=>(item.pokemon));
    dispatch(SetTypeFilterPokemonAction({type: 'name'}));
    dispatch(setListPokemon(pokemonList));
    dispatch(updateLoadingAction({status: false}))
    console.log('Pokemon results category', pokemonRequest);

}

export const catchPokemon : ActionCreator<PokemonListActionTypes> = ({pokemon} : {pokemon : PokemonListResponseItem}) => ({type: CATCH_POKEMON, payload: pokemon});



export const breakFreePokemonAction : ActionCreator<PokemonListActionTypes> = ({pokemon} : {pokemon : PokemonListResponseItem}) => ({type: BREAK_FREE_POKEMON, payload: pokemon});

export const SetTypeFilterPokemonAction: ActionCreator<PokemonListActionTypes> = ({type} : {type: string}) => ({type: SET_TYPE_FILTER, payload: type});