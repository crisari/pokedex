import { ActionCreator } from 'redux';
import http from '../../services/http';
import { PokemonDataActionTypes, PokemonInterface, SET_POKEMON_DATA } from '../types';

const setDataPokemon: ActionCreator<PokemonDataActionTypes> = (pokemon: PokemonInterface) => {
    return {type: SET_POKEMON_DATA, payload: pokemon};
} 


export function fetchInitPokemonData({url}: {url: string} ) {
    return async (dispatch: any) => {
        console.log('Url to get',url);
        const pokemonRequest = await http.get(url);
        
        console.log('Pokemon result', pokemonRequest);
        const pokemonData: PokemonInterface = {
            abilities: pokemonRequest.abilities,
            base_experience: pokemonRequest.base_experience,
            forms: pokemonRequest.forms,
            height: pokemonRequest.height,
            held_items: pokemonRequest.held_items,
            id: pokemonRequest.id,
            is_default: pokemonRequest.is_default,
            location_area_encounters: pokemonRequest.location_area_encounters,
            name: pokemonRequest.name,
            order: pokemonRequest.order,
            past_types: pokemonRequest.past_types,
            species: pokemonRequest.species,
            sprites: pokemonRequest.sprites,
            stats: pokemonRequest.stats,
            types: pokemonRequest.types,
            weight: pokemonRequest.weight,
            url
        };
        console.log('Pokemon data', pokemonData);
        return dispatch(setDataPokemon(pokemonData))
    }
}