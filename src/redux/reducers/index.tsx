
   
import { combineReducers } from 'redux';
import {pokemonViewReducer } from './pokemon.reducer';
import { pokemonListReducer } from './pokemon_list.reducer';


export const rootReducer = combineReducers({
  homeViewState: pokemonListReducer,
  pokemonViewState: pokemonViewReducer,
});

export type RootState = ReturnType<typeof rootReducer>;