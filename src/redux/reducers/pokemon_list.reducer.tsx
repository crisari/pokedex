import { PokemonListActionTypes, SET_POKEMON_LIST, PokemonListResponseItem, SET_TYPE_FILTER, SET_LOADING, SET_NEXT_PREV_PATH, CATCH_POKEMON, BREAK_FREE_POKEMON, SET_POKEMONS_AUTOCOMPLETE, PokemonItemAutocomplete } from "../types";

interface PokemonViewState {
    pokemons: PokemonListResponseItem[],
    pokemonsGot: PokemonListResponseItem[],
    pokemonsAutocomplete: PokemonItemAutocomplete[],
    typeFilter: string,
    loading: boolean,
    next: string | null,
    prev: string | null
}

const initState: PokemonViewState = {
    pokemons: [],
    pokemonsGot: [],
    pokemonsAutocomplete: [],
    typeFilter: 'name',
    loading: true,
    next: null,
    prev: null
}

export function pokemonListReducer(state: PokemonViewState = initState, action: PokemonListActionTypes){
    let _state: PokemonViewState = {...state};
    switch(action.type){
        case SET_POKEMON_LIST: 
            _state.pokemons = action.payload
            _state.loading = false;
            break;
        case SET_TYPE_FILTER : 
            _state.typeFilter = action.payload;
            break;
        case SET_LOADING : 
            _state.loading = action.payload;
            break
        case SET_NEXT_PREV_PATH:
            _state.next = action.payload.next;
            _state.prev = action.payload.prev;
            break;
        case CATCH_POKEMON:
            if(_state.pokemonsGot.find((pokemon : PokemonListResponseItem)=> pokemon.name === action.payload.name) == undefined){
                _state.pokemonsGot.push(action.payload);
            }
            break;
        case BREAK_FREE_POKEMON:
            const index = _state.pokemonsGot.findIndex((pokemon : PokemonListResponseItem) => pokemon.name === action.payload.name);
            if (index != -1){
                _state.pokemonsGot.splice(index, 1);
            }
            break;
        case SET_POKEMONS_AUTOCOMPLETE:
            _state.pokemonsAutocomplete = action.payload;
            break;
        default: 
    }
    return _state;
}