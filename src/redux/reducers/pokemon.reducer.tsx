import { PokemonDataActionTypes, PokemonInterface, SET_POKEMON_DATA } from "../types";

interface PokemonViewState {
    pokemon: PokemonInterface,
    isLoaded: boolean,
}

const initState: PokemonViewState = {
    pokemon: {
        abilities: [],
        base_experience: 0,
        forms: [],
        height: 0, 
        held_items: [],
        id: 0, 
        is_default: false,
        location_area_encounters: '',
        name: '',
        order: 0, 
        past_types: [],
        species: {name: '', url: null},
        sprites: null,
        stats: [],
        types: [],
        weight: 0,
        url: ''
        //game_indices: [],
        //moves: [],
        
    },
    isLoaded: false
}

export function pokemonViewReducer(state: PokemonViewState = initState, action: PokemonDataActionTypes){
    let _state: PokemonViewState = {...state};
    switch(action.type){
        case SET_POKEMON_DATA: {
            _state.pokemon = action.payload;
            _state.isLoaded = true
            break;
        }
        default: 
    }
    return _state;
}