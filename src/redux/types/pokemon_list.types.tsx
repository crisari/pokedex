import { PokemonInterface } from ".";

export interface PokemonListResponseItem {
    name: string,
    url: string
}

export interface NextPrevPayload {
    next: string | null, 
    prev: string | null
}


export interface PokemonListResponse {
    count: number | null, 
    next: string | null,
    previous: string | null,
    results: []
}

export interface PokemonItemAutocomplete {
    id: string,
    title: string
}

export const CATCH_POKEMON = 'CATCH_POKEMON';
export const BREAK_FREE_POKEMON = 'BREAK_FREE_POKEMON';
export const SET_LOADING = 'SET_LOADING';
export const SET_NEXT_PREV_PATH = 'SET_NEXT_PREV_PATH';
export const SET_POKEMONS_AUTOCOMPLETE = 'SET_POKEMONS_AUTOCOMPLETE';
export const SET_POKEMON_LIST = 'SET_POKEMON_LIST';
export const SET_TYPE_FILTER = 'SET_TYPE_FILTER';

interface BreakFreePokemon {
    type: typeof BREAK_FREE_POKEMON,
    payload: PokemonListResponseItem
}

interface SetPokemonsForAutocomplete {
    type: typeof SET_POKEMONS_AUTOCOMPLETE,
    payload: PokemonItemAutocomplete[]
}

interface CatchPokemonAction {
    type: typeof CATCH_POKEMON,
    payload: PokemonListResponseItem
}

interface SetLoadingStatus {
    type: typeof SET_LOADING,
    payload: boolean
}

interface SetTypeFilterPokemons {
    type: typeof SET_TYPE_FILTER,
    payload: string
}

interface SetNextPrevPath {
    type: typeof SET_NEXT_PREV_PATH,
    payload: NextPrevPayload
}

interface SetPokemonListAction {
    type: typeof SET_POKEMON_LIST,
    payload: PokemonListResponseItem[]
}

export type PokemonListActionTypes = SetPokemonListAction | SetTypeFilterPokemons | SetLoadingStatus | SetNextPrevPath | CatchPokemonAction | BreakFreePokemon | SetPokemonsForAutocomplete;