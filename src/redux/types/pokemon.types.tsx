export interface PokemonInterface {
    abilities: PokemonAbilities[],
    base_experience: number | null,
    forms: PokemonForm[] | null,
    //game_indices: Array<Object> | null,
    height: number | null, 
    held_items: Array<Object> | null,
    id: number | null, 
    is_default: Boolean | null,
    location_area_encounters: String | null,
    //moves: PokemonMoves[] | null,
    name: String | null,
    order: number | null, 
    past_types: Object[] | null,
    species: BaseNameUrl | null,
    sprites: SpritesPokemon | null,
    stats: PokemonStats[] | null,
    types: PokemonsTypes[] | null,
    weight: Number, 
    url: string
}

export interface BaseNameUrl {
    name: string | null,
    url : string | null
}
export interface PokemonsTypes {
    slot: number | null,
    type: BaseNameUrl | null
}
export interface PokemonStats {
    base_stat: number | null,
    effort: number | null,
    stat: BaseNameUrl | null
}
export interface SpritesPokemon {
    back_default: string,
    back_female: string,
    back_shiny: string,
    back_shiny_female: string,
    front_default: string,
    front_female: string,
    front_shiny: string,
    front_shiny_female: string,
    other: object | any,
    versions: object
}
export interface PokemonAbilities {
    ability: Abilitie | null,
    is_hidden: boolean | null,
    slot: number  | null
}
export interface Abilitie {
    name: string  | null,
    url: string | null
}
export interface PokemonForm {
    name: string | null,
    url: string | null
}
export interface PokemonGameIndice {
    game_index: number | null,
    version: BaseNameUrl | null
}
export interface PokemonMoves {
    move: BaseNameUrl | null,
    version_group_details: PokemonMovesVersionGroupDetails[] | null
}
export interface PokemonMovesVersionGroupDetails {
    level_learned_at: number | null,
    move_learn_method: BaseNameUrl | null,
    version_group: BaseNameUrl | null
}


export const SET_POKEMON_DATA = 'SET_POKEMON_DATA';

interface SetPokemonDataAction {
    type: typeof SET_POKEMON_DATA,
    payload: PokemonInterface
}

export type PokemonDataActionTypes = SetPokemonDataAction;