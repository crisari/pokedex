import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    buttonTab: {
        padding: 5,
        backgroundColor: 'orangeAccent'
    },
    container:  {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    containerList: {
        borderTopColor: '#eeeeee',
        borderTopWidth: 1
    },
    itemPokemon:{
        padding: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#eeeeee',
        alignItems: 'center',
        borderWidth: 1,
        borderTopWidth: 0
    },
    row: {
        flexDirection: "column",
        flexWrap: "wrap",  
    },
    p_10: {
        padding: 10
    }
});

export default styles;