import React, {FC, useEffect} from 'react';
import { Button, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { fetchInitPokemonList, fetchTypePokemonList, SetTypeFilterPokemonAction } from '../../redux';
import styles from './styles_home';

const HomeButtonsFilter: FC<{currentType: string}> = ({currentType})=>{
    const dispatch = useDispatch();

    const triggerSetFilter = ({type} : {type: string})=>{
        dispatch(SetTypeFilterPokemonAction({type}));

        //if(currentType != type){
            console.log('Typo de filtro ', type);
            if(type == 'type'){
                dispatch(fetchTypePokemonList({path: '', typeFilter: 'type'}));
            }else if(type == 'ability'){
                dispatch(fetchTypePokemonList({path: '', typeFilter: 'ability'}));
            }else if(type == 'name'){
                dispatch(fetchInitPokemonList({path: ''}));
            }
        //}
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.buttonTab}>
                <Button title={'Atrapados'} color='goldenrod' onPress={()=>triggerSetFilter({type: 'catch'})}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonTab}>
                <Button title={'Por Nombre'} color='darkorange' onPress={()=>triggerSetFilter({type: 'name'})} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonTab}>
                <Button title={'Por Tipo'} color='dodgerblue' onPress={()=>triggerSetFilter({type: 'type'})}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonTab}>
                <Button title={'Por Habilidad'} color='indigo' onPress={()=>triggerSetFilter({type: 'ability'})}/>
            </TouchableOpacity>
        </View>
    )
}

export default HomeButtonsFilter;