import React, {FC, useEffect} from 'react';
import {Button, FlatList, ScrollView, StyleSheet, Text,TouchableOpacity,View,} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import type {RootState} from './../../redux/reducers'
import { useDispatch, useSelector } from 'react-redux';
import { breakFreePokemonAction, catchPokemon, fetchInitPokemonList, fetchTypePokemonList, getAllPokemonsForAutoComplete, loadCategoryPokemons, updateLoadingAction } from '../../redux';
import { PokemonItemAutocomplete, PokemonListResponseItem } from '../../redux/types';
import styles from './styles_home';
import HomeButtonsFilter from './home_buttons_filter';
import { useNavigation } from '@react-navigation/core';
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown';

import { ActivityIndicator, IconButton } from 'react-native-paper';

const HomeScreen: FC = ()=>{
    const dispatch = useDispatch();
    const navigation = useNavigation()
    const pokemonState = useSelector((state: RootState)=> state.homeViewState);
    const {typeFilter, loading, next, prev, pokemonsGot, pokemons, pokemonsAutocomplete} : {typeFilter: string, loading: boolean, next: string | null, prev: string | null, pokemonsGot : PokemonListResponseItem[], pokemons: PokemonListResponseItem[], pokemonsAutocomplete : PokemonItemAutocomplete[]} = pokemonState;
    const FILTERS : any = {name: 'Por nombre', type: 'Por Tipo', ability: 'Por Habilidad', catch: 'Atrapados' };
    useEffect(() => {
        dispatch(fetchInitPokemonList({path: ''}))
        dispatch(getAllPokemonsForAutoComplete());
    }, [dispatch]);
    const goToPokemonDetail = ({url} : {url: string})=> navigation.navigate('PokemonDetail',{url});

    const paginationResults = ({path}: {path: string})=>{
        if(Boolean(path)) dispatch(fetchTypePokemonList({path, typeFilter}))
    }
    const _catchPokemon = ({pokemon}: {pokemon: PokemonListResponseItem}) => dispatch(catchPokemon({pokemon}));
    
    const checkPokemonCatched  = ({pokemon}: {pokemon : PokemonListResponseItem}) : boolean => {
        return  pokemonsGot.find((_pokemon: PokemonListResponseItem) => _pokemon.name === pokemon.name) == undefined;
    }
    const _breakFreePokemon = ({pokemon} : {pokemon : PokemonListResponseItem}) => dispatch(breakFreePokemonAction({pokemon}));

    const listToShow  = () : PokemonListResponseItem[] =>{
        return typeFilter === 'catch' ? pokemonsGot : pokemons;
    }

    const handleSelectedPokemon = (data: any) => {
        console.log('Selected pokemon',data);
    }
    const loadPokemonsCategory = ({url}:{url : string}) => {
        dispatch(updateLoadingAction({status: true}))
        console.log("Category", url);
        dispatch(loadCategoryPokemons({url}))
    }
    return (
        <SafeAreaView>
            <ScrollView>
            <HomeButtonsFilter currentType={typeFilter} />
            <Text>Filtrado de {FILTERS[typeFilter]}</Text>
            {/* <Text>{JSON.stringify(typeFilter)}</Text> */}
            {/* <Text>{JSON.stringify(pokemons)}</Text> */}
            {/* <Text>{JSON.stringify(next)}</Text> */}
            <AutocompleteDropdown
                clearOnFocus={false}
                closeOnBlur={true}
                closeOnSubmit={false}
                initialValue={{ id: '2' }} // or just '2'
                onSelectItem={handleSelectedPokemon}
                dataSet={pokemonsAutocomplete}
                />
            {!loading ? 
            <>
                <Text>Listado por {FILTERS[typeFilter]}</Text>
                <View style={{...styles.container, justifyContent: 'space-between'}}>
                    <IconButton icon={'arrow-collapse-left'}  onPress={()=> paginationResults({path: prev!})}/>
                    <IconButton icon={'arrow-collapse-right'} onPress={()=> paginationResults({path: next!})}/>
                </View>
                <View style={styles.containerList}>

                    {listToShow().map((pokemon: PokemonListResponseItem, index)=>(
                        <View key={'pokemon_'+index} style={styles.itemPokemon}>
                            <Text>{pokemon.name.toLocaleUpperCase()}</Text>
                            {(typeFilter == 'name' || typeFilter == 'catch') && <View>
                                {checkPokemonCatched(({pokemon})) ?
                                        <Button color='mediumpurple' title={'ATRAPAR'} onPress={()=>_catchPokemon({pokemon})} /> 
                                        :
                                        <Button color='green' title={'LIBERAR'} onPress={()=>_breakFreePokemon({pokemon})} /> 
                                }
                                <Button title={'VER'} onPress={()=>goToPokemonDetail({url: pokemon.url})} /> 
                            </View>}
                            {(typeFilter == 'type' || typeFilter == 'ability') && <Button title={'VER POKEMONES'} onPress={()=>loadPokemonsCategory({url: pokemon.url})} />}
                            
                        </View>
                    ))}
                </View>
            </>
                : 
                <View>
                    <Text>Cargando informacion...</Text>
                    <ActivityIndicator style={{marginTop: 20}}  animating={true} color='coral' />
                </View> 
            }
            
            </ScrollView>
        </SafeAreaView>
    )
}




export default HomeScreen;