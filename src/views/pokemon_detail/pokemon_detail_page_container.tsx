import React, {FC, useEffect} from 'react';
import type {RootState} from './../../redux/reducers'
import { useDispatch, useSelector } from 'react-redux';
import { Image, ScrollView, Text, View, Button, StyleSheet } from 'react-native';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/core';
import { SafeAreaView } from 'react-native-safe-area-context';
import { BaseNameUrl, PokemonAbilities, PokemonForm, PokemonInterface, PokemonStats, PokemonsTypes } from '../../redux/types';
import { ActivityIndicator } from 'react-native-paper';
import styles from '../home/styles_home';
import { fetchInitPokemonData } from '../../redux';
import { RootStackParamList } from '../../../App';
import { List } from 'react-native-paper';


const PokemonDetailContainer : FC = (props)=>{
    const navigation = useNavigation();
    const route = useRoute<RouteProp<RootStackParamList, 'PokemonDetail'>>();
    const dispatch = useDispatch();
    const pokemonDataState = useSelector((state: RootState)=> state.pokemonViewState);
    const {isLoaded, pokemon} : {isLoaded: boolean, pokemon: PokemonInterface} = pokemonDataState;
    

    useEffect(()=>{
        dispatch(fetchInitPokemonData({url: route.params.url}))
    },[dispatch]);
    //console.log('Props',props)
    console.log('Navigation data',navigation, route);
    return (
        <SafeAreaView>
            <ScrollView>
                {!isLoaded ?
                <View>
                    <Text>Cargando informacion...</Text>
                    <ActivityIndicator style={{marginTop: 20}}  animating={true} color='coral' />
                </View> 
                :
                <View>
                    <View style={{...styles.row, alignItems: 'center', alignContent: 'center'}}>
                        <Image style={{height: 100, width: 100}} source={{uri: pokemon.sprites?.front_default}}/>
                        <Text style={{fontSize: 30, textTransform: 'capitalize'}}>{pokemon.name}</Text>
                    </View> 
                    <View style={styles.container}>
                        <List.Section style={{flex:1}}>
                            <List.Subheader>Informacion</List.Subheader>
                            <List.AccordionGroup>
                                <List.Accordion title='Habilidades' id='1'>
                                    {pokemon.abilities.map((el: PokemonAbilities, index)=>
                                        <List.Item style={{paddingLeft: 50}} title={el.ability?.name?.toLocaleUpperCase()} key={'hab'+index}/>
                                    )}
                                </List.Accordion>
                            </List.AccordionGroup>
                            <List.AccordionGroup>
                                <List.Accordion title='Formas' id='4'>
                                    {pokemon.forms?.map((el: PokemonForm, index)=>
                                        <List.Item style={{paddingLeft: 50}} title={el.name?.toLocaleUpperCase()} key={'hab'+index}/>
                                    )}
                                </List.Accordion>
                            </List.AccordionGroup>
                            <List.Item title="Altura" description={pokemon.height} />
                            <List.Item title="Peso" description={pokemon.weight} />
                            <List.Item title="Experiencia Base" description={pokemon.base_experience} />
                            <List.Item title="ID" description={pokemon.id} />
                            <List.Item title="IS DEFAULT" description={pokemon.is_default ? 'TRUE' : 'FALSE'} />
                            <List.Item title="ORDER" description={pokemon.order} />
                        </List.Section>
                        <List.Section style={{flex:1}}>
                            <List.Subheader></List.Subheader>
                            <List.AccordionGroup>
                                <List.Accordion title='Imagenes' id='2'>
                                    <View style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.back_default}} />
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.back_female}} />
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.back_shiny}} />
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.back_shiny_female}} />
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.front_default}} />
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.front_female}} />
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.front_shiny}} />
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.front_shiny_female}} />
                                        <Image style={style.imgPk}  source={{uri: pokemon.sprites?.front_shiny_female}} />
                                    </View>
                                        
                                </List.Accordion>
                            </List.AccordionGroup>
                            <List.AccordionGroup>
                                <List.Accordion title='Tipo Pokemon' id='3'>
                                    {pokemon.types?.map((el: PokemonsTypes, index)=>
                                        <List.Item style={{paddingLeft: 50}} title={el.type?.name?.toLocaleUpperCase()} key={'hab'+index}/>
                                    )}
                                </List.Accordion>
                            </List.AccordionGroup>
                            <List.AccordionGroup>
                                <List.Accordion title='En Batalla' id='5'>
                                    {pokemon.stats?.map((el: PokemonStats, index)=>
                                        <List.Item style={{paddingLeft: 10}} title={el.stat?.name?.toLocaleUpperCase()} description={el.base_stat} key={'hab'+index}/>
                                    )}
                                </List.Accordion>
                            </List.AccordionGroup>
                        </List.Section>
                    </View>
                </View>
                }
                {/* <Text>{JSON.stringify(pokemonDataState)}</Text> */}
            </ScrollView>
        </SafeAreaView>
    )
}
const style = StyleSheet.create({
    imgPk: {height: 40, width: 40}
})

export default PokemonDetailContainer;