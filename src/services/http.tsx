import axios from "axios";

export const URL_BASE : string = "https://pokeapi.co/api/v2/";

const instance = axios.create({
    baseURL: URL_BASE,
    headers: {
      "Content-type": "application/json"
    }
});

const http = {
    get: async (path: string ,params: object = {})=>{
        let {status, data} = await instance.get(path,{params})
        if(status===200){   
            return data;
        }else{
            return {error:'ERROR_API', path: path, dataResponse: data.content};
        }
    },
    post : async (path: string, params={})=>{        
        let {status, data} = await instance.post(path, params);
        if(status===200){
            return data;
        } else {
            return {error:'ERROR_API', path: path, dataResponse: data.content};
        }
    }
       
}
export default http;