/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import { Provider } from 'react-redux';
import { store } from './src/redux';
import { DarkTheme, NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { DefaultTheme, Provider as PaperProvider, DarkTheme as Dark } from 'react-native-paper';
import HomeScreen from './src/views/home/home_container';
import PokemonDetailContainer from './src/views/pokemon_detail/pokemon_detail_page_container';
export type RootStackParamList = {
  Home: undefined;
  PokemonDetail: {url: string};
};

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}


const RooStack = createNativeStackNavigator<RootStackParamList>();

const App = () => {


  return (
    <Provider store={store}>
        <PaperProvider theme={Dark}>
          <NavigationContainer theme={DarkTheme}>
            <RooStack.Navigator initialRouteName={'Home'} >
              <RooStack.Screen name={'Home'} component={HomeScreen} options={{title: 'PokeDex'}} />
              <RooStack.Screen name={'PokemonDetail'} options={{title: 'Detalle Pokemon'}} component={PokemonDetailContainer} initialParams={{url: ''}}/>
            </RooStack.Navigator>
          </NavigationContainer>
        </PaperProvider>
    </Provider>
  );
};

export default App;
